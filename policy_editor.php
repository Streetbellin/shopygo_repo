<?php
include_once '../../include/session.php';
include("../../include/globals.php");
include("header.php");
include("../../include/connect.php");
include 'mainmenu.php';
include '../logincheck.php';
include 'rightbar.php';
include 'leftmenu.php';
include_once '../../logincheckfunction.php';


$psiname=$hostname;

$statusdata1='';
$query2 = "SELECT * FROM `legal_pages` where `psiname`='$psiname'";
 
    $results =  $mysqli->query($query2);
    $row_cnt = $results->num_rows;
	 while ($rows = $results->fetch_array(MYSQLI_BOTH))
     {
        $refund= $rows['returnrefund_policy'];
        $rstatus=$rows['returnrefund_active'];
        $shippingpage=$rows['shipping_policy'];
        
        $shstatus=$rows['shipping_active'];
         $privacypage= $rows['privacy_policy'];
         
          $pstatus= $rows['privacy_active'];
         
         $termspage=$rows['terms_policy'];
         
         $tstatus=$rows['terms_active'];
         
         $faq=$rows['faq_help'];
         
            $fstatus=$rows['faq_active'];
            
            
            
 } 
 
?>
<link rel="stylesheet" type="text/css" href="files/assets/css/pages.css">

<script type="text/javascript" src="files/bower_components/jquery/js/jquery.min.js"></script>

<div class="pcoded-content">
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="feather icon-box bg-c-blue"></i>
                    <div class="d-inline">
                        <h5>Policy</h5>
                        <span>Policy Editor</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class=" breadcrumb breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="dashboard.php"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="policy_editor.php">Policy Editor</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> 
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">

                <div class="page-body">
                    <div class="row">
							<div class="col-md-12 col-xl-3">
								<div class="card">
									<div class="card-header">
										<h5>Legal Page</h5>
									</div>
									<div class="card-block">
										<p>You can create your own legal pages or create them from templates and customize them. <br>The templates aren't legal advice and need to be customized for our your store.</p>
										<p>To keep your store secure, it's not possible to add audio, videos or media widgets.</p>
										<p>By using these templates you agree that you've read and agreed to the disclaimer.</p>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-xl-9">
                                               
                            
                                <div>
                                    <form id="frmreturmpolicy" name="frmreturmpolicy" method="post" >
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Return Policy</h5>
                                                <!--<span style="display: block !important;">This editor helps you to edit your content easily</span>-->
                                            </div>
                                            <div class="card-block">
												<p class="f-14 text-info">This editor helps you to edit your content easily</p>
                                                <textarea id="txtreturn" name="editorcontent1">
													<?php if(isset( $refund)) {?> <?php echo $refund ?><?php } ?>
                                                </textarea>
                                                <div class="checkbox-fade fade-in-primary text-left m-t-10">
													<label>
														
														
														<?php if($rstatus==1): ?> 
                                                           <input type="checkbox" name="status1" value="1" checked=""  />
                                                               <?php else: ?>
                                                            <input type="checkbox" name="status1" value="1"   />
                                                            <?php endif; ?>
														
														
														<span class="cr">
															<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
														</span>
														<span>Status</span>
													</label>
												</div>
                                                <input type="hidden" id="psiname" name="psiname" value="<?php echo $hostname; ?>"/>
                                                
                                                
												<div class="text-right" style="float:right;">
                                                
												<input type="button" id="update1" class="btn-sm btn-primary waves-effect waves-light m-t-10" value="Save"/>
                                                    
                                   
												</div>
                                            </div>
                                        </div>
                                    </form>
                                    
                                </div>
                                
                         
                                
                               <div>
                                
                                    <form id="frmprivacypolicy"  name="frmprivacypolicy" method="post">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Privacy Policy</h5>
                                                <!--<span style="display: block !important;">This editor helps you to edit your content easily</span>-->
                                            </div>
                                            <div class="card-block">
												<p class="f-14 text-info">This editor helps you to edit your content easily</p>
                                                <textarea id="txtprivacy" name="txtprivacy">
												<?php echo $privacypage; ?>
                                                </textarea>
                                               <div class="checkbox-fade fade-in-primary text-left m-t-10">
													<label>
														
                                                    <?php if($pstatus==1): ?> 
                                                      <input type="checkbox" name="status2" value="1" checked=""  />
                                                      <?php else: ?>
                                                      <input type="checkbox" name="status2" value="1"   />
                                                        <?php endif; ?>
														<span class="cr">
															<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
														</span>
														<span>Status</span>
													</label>
												</div>
                                                 <input type="hidden" id="psiname1" name="psiname1" value="<?php echo $hostname; ?>"/>
                                                
												<div class="text-right" style="float:right;">
													
                                                    
                                                    	<input type="button" id="update2" class="btn-sm btn-primary waves-effect waves-light m-t-10" value="Save"/>
												</div>
                                            </div>
                                        </div>
                                    </form>       
                                </div> 
                
                                
                                
                                
                                <div>
                                
                                    <form id="frmtermspolicy" name="frmreturmpolicy" method="post" >
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Terms & Conditions</h5>
                                                <!--<span style="display: block !important;">This editor helps you to edit your content easily</span>-->
                                            </div>
                                            <div class="card-block">
												<p class="f-14 text-info">This editor helps you to edit your content easily</p>
                                                <textarea id="txtterms" name="txtterms">
												 <?php echo $termspage ?>
                                                </textarea>
												<div class="checkbox-fade fade-in-primary text-left m-t-10">
													<label>
														
                                                         <?php if($tstatus==1): ?> 
                                                    <input type="checkbox" name="status3" value="1" checked=""  />
                                                    <?php else: ?>
                                                  <input type="checkbox" name="status3" value="1"   />
                                                     <?php endif; ?>
                                                        
                                                        
														<span class="cr">
															<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
														</span>
														<span>Status</span>
													</label>
												</div>
                                                  <input type="hidden" id="psiname2" name="psiname2" value="<?php echo $hostname; ?>"/>
												<div class="text-right" style="float:right;">
												
                                                    	<input type="button" id="update3" class="btn-sm btn-primary waves-effect waves-light m-t-10" value="Save"/>
												</div>                                                       
                                            </div>
                                        </div>
                                    </form>       
                                </div>  
                                
                
                                <div>
                                   
                                    <form id="frmshippingspolicy" name="frmshippingspolicy" method="post">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Shipping Policy</h5>
                                                <!--<span style="display: block !important;">This editor helps you to edit your content easily</span>-->
                                            </div>
                                            <div class="card-block">
												<p class="f-14 text-info">This editor helps you to edit your content easily</p>
                                                <textarea id="txtshipping" name="txtshipping">
												 <?php echo $shippingpage ?>
                                                </textarea>
												<div class="checkbox-fade fade-in-primary text-left m-t-10">
													<label>
													<?php if($shstatus==1): ?> 
                                                       <input type="checkbox" name="status4" value="1" checked=""  />
                                                       <?php else: ?>
                                                       <input type="checkbox" name="status4" value="1"   />
                                                       <?php endif; ?>
														<span class="cr">
															<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
														</span>
														<span>Status</span>
													</label>
												</div>
                                                <input type="hidden" id="psiname3" name="psiname3" value="<?php echo $hostname; ?>"/>
												<div class="text-right" style="float:right;">
													<input type="button" id="update4" class="btn-sm btn-primary waves-effect waves-light m-t-10" value="Save"/>
												</div>
                                            </div>
                                        </div>
                                    </form>       
                                </div> 
                                
                                
                                <div>
                                        
                                    <form id="frmfaqpolicy" name="frmfaqpolicy" method="post">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>FAQ & Help Center</h5>
                                                <!--<span style="display: block !important;">Thiss editor helps you to edit your content easily</span>-->
                                            </div>
                                            <div class="card-block">
												<p class="f-14 text-info">This editor helps you to edit your content easily</p>
                                                <textarea id="txtfaq" name="txtfaq">
												<?php echo $faq?>
                                                </textarea>
												<div class="checkbox-fade fade-in-primary text-left m-t-10">
													<label>
  											     <?php if($fstatus==1): ?> 
                                                    <input type="checkbox" name="status5" value="1" checked=""  />
                                                    <?php else: ?>
                                                    <input type="checkbox" name="status5" value="1"   />
                                                    <?php endif; ?>
														<span class="cr">
															<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
														</span>
														<span>Status</span>
													</label>
												</div>
                                                <input type="hidden" id="psiname4" name="psiname4" value="<?php echo $hostname; ?>"/>
												<div class="text-right" style="float:right;">
												
                                                    	<input type="button" id="update5" class="btn-sm btn-primary waves-effect waves-light m-t-10" value="Save"/>
												</div>
                                            </div>
                                        </div>
                                    </form>       
                                </div>      
                            
                            

                            
                            
                            
                                
                                   
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php include'footer.php'; ?>
<script src="files/assets/pages/wysiwyg-editor/js/tinymce.min.js" type="text/javascript"></script>
<script src="files/assets/pages/wysiwyg-editor/wysiwyg-editor.js" type="text/javascript"></script>

<script type="text/javascript">

tinymce.init({
  selector: 'textarea#basic-example',
  height: 500,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: 'undo redo | formatselect | ' +
  'bold italic backcolor | alignleft aligncenter ' +
  'alignright alignjustify | bullist numlist outdent indent | ' +
  'removeformat | help',
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});


</script>

<script type="text/javascript">
$(document).ready(function(){ 
   
    $('#update1').click(function(e){
    e.preventDefault();
      var url = window.location.origin;
    
    var page=url +'/main/services/returnpolicy_ajax.php';
     tinyMCE.triggerSave();                           
   var formData= $('#frmreturmpolicy').serialize();
 
    
         $.ajax({
			url:page,
			method: "POST",              
			data: formData,
			dataType:"json",
			success: function(data){   
			 
				alert('Updated Return Policy Successfully');
				
			}
	});		
	return false;      
         
});

 $('#update2').click(function(e){
    e.preventDefault();
       var url = window.location.origin;
     
    var page=url +'/main/services/privacypolicy_ajax.php';
     tinyMCE.triggerSave();                            
     var formData= $('#frmprivacypolicy').serialize();
    
     
    	$.ajax({
			url:page,
			method: "POST",              
			data: formData,
			dataType:"json",
			success: function(data){   
			 
				alert('Updated Privacy Policy Successfully');
				
			}
	});		
	return false;      
         
});

  $('#update3').click(function(e){
    e.preventDefault();
       var url = window.location.origin;
    
    var page=url +'/main/services/terms_ajax.php';  
     tinyMCE.triggerSave();                                  
   var formData= $('#frmtermspolicy').serialize();
     
    	$.ajax({
			url:page,
			method: "POST",              
			data: formData,
			dataType:"json",
			success: function(data){   
		
				alert('Updated Terms Policy Successfully');
				
			}
	});		
	return false;      
         
});


    $('#update4').click(function(e){
    e.preventDefault();
       var url = window.location.origin;
     
    var page=url +'/main/services/shipping_ajax.php'; 
     tinyMCE.triggerSave();                          
   var formData= $('#frmshippingspolicy').serialize();
    
    	$.ajax({
			url:page,
			method: "POST",              
			data: formData,
			dataType:"json",
			success: function(data){   
			 
				alert('Updated Shipping Policy Successfully');
				
			}
	});		
	return false;      
         
});

$('#update5').click(function(e){
    e.preventDefault();
       var url = window.location.origin;
     
    var page=url +'/main/services/faq_ajax.php';  
      tinyMCE.triggerSave();                          
   var formData= $('#frmfaqpolicy').serialize();
     
    	$.ajax({
			url:page,
			method: "POST",              
			data: formData,
			dataType:"json",
			success: function(data){   
			 
				alert('Updated FAQ Successfully');
				
			}
	});		
	return false;      
         
});


});
</script> 





 